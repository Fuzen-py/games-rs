//! Rock Paper Scissors
//! ```
//! use games::rps::*;
//! // RpsGame::new_game("rock".parse())?; Can be used
//! let rps = RpsGame::new_game(Weapon::Rock);
//! match rps.into() {
//!     GameResult::Win {player, robo} => println!("{} beats {}, you win!", player, robo),
//!     GameResult::Lose {player, robo} => println!("{} is weaker than {}, you lose!", player, robo),
//!     GameResult::Draw { .. } => println!("Too bad the objective wasn't to guess the other parties weapon. This is a draw!")
//! }
//! ```

mod game;
mod weapons;
pub use self::game::{GameResult, RpsGame};
pub use self::weapons::{Weapon, WeaponParseError};
