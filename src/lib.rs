#![doc(html_root_url = "https://docs.rs/games-rs/0.3.1")]
//! Pre implemented games
#![deny(
    missing_copy_implementations,
    missing_debug_implementations,
    missing_docs,
    trivial_casts,
    trivial_numeric_casts,
    unused_extern_crates,
    const_err,
    deprecated,
    overflowing_literals,
    unreachable_patterns,
    unused_must_use,
    unused_mut,
    while_true
)]
#![warn(unreachable_pub, variant_size_differences)]

pub mod blackjack;
pub mod cards;
pub mod coin_toss;
pub mod color;
pub mod deck;
// Static deck of cards
pub(crate) mod deck_of_cards;
pub mod errors;
pub mod rps;
pub mod slot_machine;

pub mod solitaire; // TODO: Improve performance

#[cfg(feature = "small_rng")]
use rand::SeedableRng;

#[cfg(not(feature = "small_rng"))]
pub(crate) fn get_rng() -> rand::rngs::ThreadRng {
    rand::thread_rng()
}
#[cfg(feature = "small_rng")]
pub(crate) fn get_rng() -> rand::rngs::SmallRng {
    // Once per thread
    let mut thread_rng = rand::thread_rng();
    rand::rngs::SmallRng::from_rng(&mut thread_rng).expect("Assume this won't fail.")
}

// TODO: Cleanup codebase
// TODO: each game a feature
