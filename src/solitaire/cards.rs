/// Solitaire card
#[repr(C)]
#[derive(
    Copy, Clone, Debug, PartialEq, Eq, serde::Serialize, serde::Deserialize, Ord, PartialOrd, Hash,
)]
pub struct SolitaireCard {
    pub(crate) card: crate::cards::StandardCard,
    pub(crate) face_down: bool,
}

impl Into<crate::cards::StandardCard> for SolitaireCard {
    fn into(self) -> crate::cards::StandardCard {
        self.card
    }
}

impl Into<SolitaireCard> for crate::cards::StandardCard {
    fn into(self) -> SolitaireCard {
        SolitaireCard {
            card: self,
            face_down: true,
        }
    }
}

impl SolitaireCard {
    /// Create a new solitaire card face down
    pub const fn new_down(card: crate::cards::StandardCard) -> Self {
        Self {
            card,
            face_down: true,
        }
    }
    /// Create a new solitaire card face up
    pub const fn new_up(card: crate::cards::StandardCard) -> Self {
        Self {
            card,
            face_down: false,
        }
    }
    /// Flip the card over
    pub fn flip(&mut self) {
        self.face_down = !self.face_down;
    }
    /// Returns if the card is face down or not
    pub const fn is_facedown(self) -> bool {
        self.face_down
    }
    /// Returns if the card is face up or not
    pub const fn is_faceup(self) -> bool {
        !self.face_down
    }
    /// Return the card
    pub const fn card(self) -> crate::cards::StandardCard {
        self.card
    }
}
