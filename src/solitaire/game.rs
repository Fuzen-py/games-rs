use crate::solitaire::{
    errors::{SError, SolitaireError},
    SolitaireCard,
};

/// Area of cards
#[repr(C)]
#[derive(
    Copy, Clone, Debug, serde::Serialize, serde::Deserialize, Eq, PartialEq, Ord, PartialOrd, Hash,
)]
pub enum Area {
    /// Stockpile of cards
    Stockpile,
    /// Waste pile
    Waste,
    /// Tablue (amount: 0 is all)
    Tablue {
        /// Tablue row (0-7)
        row: usize,
        /// Amount of cards to move
        /// If amount is 0, selects the whole row
        /// If amount is > 1, selects cards from the top of stack up to the number
        amount: usize,
    },
    /// Foundation stack
    Foundation {
        /// Foundation row
        row: usize,
    },
}

/// Solitaire
#[derive(
    Clone, Debug, serde::Serialize, serde::Deserialize, Eq, PartialEq, Ord, PartialOrd, Hash,
)]
pub struct Solitaire {
    // Number of moves made
    i_moves: u32,
    /// stockpile of cards
    pub stockpile: crate::deck::Deck<crate::cards::StandardCard>,
    /// Waste pile
    pub waste: Vec<crate::cards::StandardCard>,
    /// Table
    pub tablue: [Vec<SolitaireCard>; 7],
    /// Foundation pile
    pub foundation: [Vec<SolitaireCard>; 4],
}

impl Solitaire {
    /// New solitaire game
    pub fn new() -> Self {
        let mut deck = crate::deck::Deck::default();
        deck.shuffle();
        let mut cards = deck.draw_many(28);
        Solitaire {
            i_moves: 0,
            stockpile: deck,
            waste: Vec::with_capacity(47),
            tablue: [
                // 0 Down, 1 Up
                vec![SolitaireCard::new_up(cards.remove(0))],
                // 1 Down, 1 Up
                vec![
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_up(cards.remove(0)),
                ],
                // 2 Down, 1 Up
                vec![
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_up(cards.remove(0)),
                ],
                // 3 Down, 1 Up
                vec![
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_up(cards.remove(0)),
                ],
                // 4 Down, 1 Up
                vec![
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_up(cards.remove(0)),
                ],
                // 5 Down, 1 Up
                vec![
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_up(cards.remove(0)),
                ],
                // 6 Down, 1 Up
                vec![
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_down(cards.remove(0)),
                    SolitaireCard::new_up(cards.remove(0)),
                ],
            ],
            foundation: [
                Vec::with_capacity(13),
                Vec::with_capacity(13),
                Vec::with_capacity(13),
                Vec::with_capacity(13),
            ],
        }
    }
    /// Move a card from stockpile to waste pile,
    /// if stockpile is empty, move waste to stockpile and draw card
    ///
    /// # Errors
    //
    /// Errors if there is no more cards to draw
    /// Can also error if moves exceed u32
    pub fn draw_card(&mut self) -> SError<crate::cards::StandardCard> {
        self.i_moves = self
            .i_moves
            .checked_add(1)
            .ok_or(SolitaireError::OutOfMoves)?;
        Ok(match self.stockpile.draw() {
            Ok(card) => {
                self.waste.push(card);
                card
            }
            Err(_) => {
                self.stockpile = self.waste.drain(..).collect();
                self.stockpile
                    .draw()
                    .map_err(|_| SolitaireError::OutOfCardsError)?
            }
        })
    }
    /// Number of moves performed
    pub fn moves(&self) -> u32 {
        self.i_moves
    }

    /// Check if the request is within a valid range
    ///
    /// # Errors
    ///
    /// Errors if the card doesnt fit the range
    ///
    /// Target rules:
    /// - Foundation
    ///     - The row must be less than 4
    /// - Stockpile
    ///     - Waste must not be empty
    ///     - Stockpile must not be empty
    /// - Waste
    ///     - Waste must not be empty
    /// - Tablue
    ///     - Row must be less than 7
    ///     - The requested amount must be less than or equal to the number of faceup cards
    ///     - The tablue must be all faceup if requested amount is 0 (All)
    ///
    /// Destination:
    /// - Foundation
    ///     - Row less than 4
    /// - Tablue
    ///     - Row less than 7
    /// - Stockpile OR Waste
    ///     - Always ok
    fn valid_range(&self, target: Area, destination: Area) -> SError<()> {
        let valid = match target {
            Area::Foundation { row } => row < 4,
            Area::Stockpile => !(self.waste.is_empty() & self.stockpile.is_empty()),
            Area::Waste => !(self.waste.is_empty()),
            Area::Tablue { row, amount } => {
                row < 7
                    && (amount <= self.tablue[row].iter().filter(|c| c.is_faceup()).count()
                        || (amount == 0 && self.tablue[row].iter().all(|c| c.is_faceup())))
            }
        };
        if !valid {
            return Err(SolitaireError::OutOfRangeError);
        };
        let valid = match destination {
            Area::Foundation { row } => row < 4,
            Area::Tablue { row, .. } => row < 7,
            Area::Stockpile | Area::Waste => true,
        };
        if valid {
            Ok(())
        } else {
            Err(SolitaireError::OutOfRangeError)
        }
    }
    /// Peek at the next card
    ///
    /// # Errors
    ///
    /// Errors if there is no cards to peek at from either
    /// the stockpile or waste
    pub fn peek(&self) -> SError<crate::cards::StandardCard> {
        if self.stockpile.is_empty() {
            if let Some(card) = self.waste.last() {
                Ok(*card)
            } else {
                Err(SolitaireError::OutOfRangeError)
            }
        } else if let Some(card) = self.stockpile.peek() {
            Ok(*card)
        } else {
            Err(SolitaireError::OutOfRangeError)
        }
    }

    /// Move card(s) from target -> Destination
    ///
    /// # Errors
    ///
    /// First Check phase:
    ///     /// Target rules:
    /// - Foundation
    ///     - The row must be less than 4
    /// - Stockpile
    ///     - Waste must not be empty
    ///     - Stockpile must not be empty
    /// - Waste
    ///     - Waste must not be empty
    /// - Tablue
    ///     - Row must be less than 7
    ///     - The requested amount must be less than or equal to the number of faceup cards
    ///     - The tablue must be all faceup if requested amount is 0 (All)
    ///
    /// Destination:
    /// - Foundation
    ///     - Row less than 4
    /// - Tablue
    ///     - Row less than 7
    /// - Stockpile OR Waste
    ///     - Always ok
    ///
    /// Second Phase:
    ///
    /// Target Stockpile:
    /// - Stockpile (Not Allowed)
    /// - Waste
    ///     - Failed to draw from stockpile
    /// - Foundation
    ///     - Card(s) not allowed to be stacked (Solitaire Rules)
    /// - Tablue
    ///     - Card(s) not allowed to be stacked (Solitaire Rules)
    ///
    ///
    /// Target Waste:
    /// - Stockpile (Never allowed)
    /// - Waste (Never allowed)
    /// - Foundation
    ///     - Card(s) not allowed to be stacked (Solitaire Rules)
    /// - Tablue
    ///     - Card(s) not allowed to be stacked (Solitaire Rules)
    ///
    ///
    /// Target Tablue:
    /// - Stockpile (Never allowed)
    /// - Waste (Never allowed)
    /// - Tablue
    ///     - Card(s) not allowed to be stacked (Solitaire Rules)
    /// - Foundation
    ///     - Card(s) not allowed to be stacked (Solitaire Rules)
    ///
    ///
    /// Target Foundation:
    /// - Waste (Never allowed)
    /// - Stockpile (Never allowed)
    /// - Foundation (Never allowed)
    /// - Tablue
    ///     - Card(s) not allowed to be stacked (Solitaire Rules)
    ///
    ///  Can also error if moves exceed the max of u32
    pub fn move_to(&mut self, target: Area, destination: Area) -> SError<()> {
        self.valid_range(target, destination)?; // Validate movement range
        let mut drawed_card = false;
        match target {
            Area::Stockpile => match destination {
                Area::Waste => {
                    self.waste.push(
                        self.stockpile
                            .draw()
                            .map_err(|_| SolitaireError::IllegalMovementError)?,
                    );
                    Ok(())
                }
                Area::Stockpile => Err(SolitaireError::IllegalMovementError),
                Area::Foundation { row } => {
                    let stackable = self
                        .peek()
                        .ok()
                        .map(|top| {
                            let bot = self.foundation[row].last().copied();
                            crate::solitaire::utils::can_stack(
                                bot,
                                SolitaireCard {
                                    card: top,
                                    face_down: false,
                                },
                                true,
                            )
                        })
                        .unwrap_or(false);
                    if stackable {
                        drawed_card = true;
                        let card = SolitaireCard {
                            card: self.draw_card()?,
                            face_down: false,
                        };
                        self.foundation[row].push(card);
                        Ok(())
                    } else {
                        Err(SolitaireError::IllegalMovementError)
                    }
                }
                Area::Tablue { row, .. } => {
                    let stackable = self
                        .peek()
                        .ok()
                        .map(|top| {
                            let bot = self.tablue[row].last().copied();

                            crate::solitaire::utils::can_stack(
                                bot,
                                SolitaireCard {
                                    card: top,
                                    face_down: false,
                                },
                                false,
                            )
                        })
                        .unwrap_or(false);
                    if stackable {
                        drawed_card = true;
                        let card = SolitaireCard {
                            card: self.draw_card()?,
                            face_down: false,
                        };
                        self.tablue[row].push(card);
                        Ok(())
                    } else {
                        Err(SolitaireError::IllegalMovementError)
                    }
                }
            },
            Area::Waste => match destination {
                Area::Stockpile | Area::Waste => Err(SolitaireError::IllegalMovementError),
                Area::Foundation { row } => {
                    let stackable = self
                        .waste
                        .last()
                        .map(|top| {
                            let top = SolitaireCard {
                                card: *top,
                                face_down: false,
                            };
                            let bot = self.foundation[row].last().copied();
                            crate::solitaire::utils::can_stack(bot, top, true)
                        })
                        .unwrap_or(false);
                    if stackable {
                        self.foundation[row].push(SolitaireCard {
                            card: self.waste.remove(self.waste.len() - 1),
                            face_down: false,
                        });
                        Ok(())
                    } else {
                        Err(SolitaireError::IllegalMovementError)
                    }
                }
                Area::Tablue { row, .. } => {
                    let stackable = self
                        .waste
                        .last()
                        .map(|top| {
                            let top = SolitaireCard {
                                card: *top,
                                face_down: false,
                            };
                            let bot = self.tablue[row].last().copied();
                            crate::solitaire::utils::can_stack(bot, top, false)
                        })
                        .unwrap_or(false);
                    if stackable {
                        self.tablue[row].push(SolitaireCard {
                            card: self.waste.remove(self.waste.len() - 1),
                            face_down: false,
                        });
                        Ok(())
                    } else {
                        Err(SolitaireError::IllegalMovementError)
                    }
                }
            },
            Area::Tablue { row, amount } => match destination {
                Area::Stockpile | Area::Waste => Err(SolitaireError::IllegalMovementError),
                Area::Tablue {
                    row: destination_row,
                    ..
                } => {
                    let stack: Vec<SolitaireCard> = if amount == 0 {
                        // If any card is face down, cant move the whole tablue
                        if self.tablue[row].iter().any(|c| c.is_facedown()) {
                            return Err(SolitaireError::IllegalMovementError);
                        }
                        self.tablue[row].drain(0..)
                    } else {
                        let l = self.tablue[row].len();
                        self.tablue[row].drain(l - amount..l)
                    }
                    .collect();
                    let stackable = stack
                        .get(0)
                        .map(|c| {
                            crate::solitaire::utils::can_stack(
                                self.tablue[destination_row].last().copied(),
                                *c,
                                false,
                            )
                        })
                        .unwrap_or(false);
                    if stackable {
                        self.tablue[destination_row].extend(stack);
                        Ok(())
                    } else {
                        Err(SolitaireError::IllegalMovementError)
                    }
                }
                Area::Foundation {
                    row: destination_row,
                } => {
                    if row >= 7 || destination_row >= 7 {
                        return Err(SolitaireError::OutOfRangeError);
                    };
                    let stackable = self.tablue[0]
                        .last()
                        .map(|top| {
                            self.foundation[destination_row]
                                .last()
                                .map(|bot| {
                                    crate::solitaire::utils::can_stack(Some(*bot), *top, true)
                                })
                                .unwrap_or_else(|| {
                                    crate::solitaire::utils::can_stack(None, *top, true)
                                })
                        })
                        .unwrap_or(false);
                    if stackable {
                        self.foundation[destination_row]
                            .push(self.tablue[row].remove(self.tablue[row].len() - 1));
                        Ok(())
                    } else {
                        Err(SolitaireError::IllegalMovementError)
                    }
                }
            },
            Area::Foundation { row } => match destination {
                Area::Waste | Area::Stockpile | Area::Foundation { .. } => {
                    Err(SolitaireError::IllegalMovementError)
                }
                Area::Tablue {
                    row: destination_row,
                    ..
                } => {
                    let stackable = self.foundation[row]
                        .last()
                        .map(|top| {
                            let bot = self.tablue[destination_row].last().copied();
                            crate::solitaire::utils::can_stack(bot, *top, false)
                        })
                        .unwrap_or(false);
                    if stackable {
                        self.tablue[destination_row]
                            .push(self.foundation[row].remove(self.foundation[row].len() - 1));
                        Ok(())
                    } else {
                        Err(SolitaireError::IllegalMovementError)
                    }
                }
            },
        }?;
        if !drawed_card {
            // Draw card increments move as well
            self.i_moves = self
                .i_moves
                .checked_add(1)
                .ok_or(SolitaireError::OutOfMoves)?;
        }
        Ok(())
    }

    /// Check if the solitaire game is a win
    pub fn game_won(&self) -> bool {
        // TODO: More complex game state checker
        if !self.stockpile.is_empty() {
            return false;
        }
        if !self.waste.is_empty() {
            return false;
        }
        if self.tablue.iter().all(|tablue_row| tablue_row.is_empty()) {
            return false;
        }
        if self
            .foundation
            .iter()
            .all(|foundation_row| foundation_row.len() == 13)
        {
            return true;
        }
        false
    }
    // TODO: Implement display method
}

impl Default for Solitaire {
    fn default() -> Self {
        Self::new()
    }
}
