//! Solitaire Card Game
//! ```
//! use games::solitaire::*;
//! let mut solitaire_game = Solitaire::new();
//! // Attempt to move a card from StockPile -> Tablue, Row 1
//! match solitaire_game.move_to(Area::Stockpile, Area::Tablue{row:1, amount: 1}) {
//!     Ok(_) => (),
//!     Err(SolitaireError::IllegalMovementError) => {solitaire_game.draw_card().ok();}, // Not a valid, lets draw a card
//!     Err(SolitaireError::OutOfCardsError) => {return;} // Shouldn't be out of cards
//!
//!     _ => unreachable!()
//! }
//! // View total moves made
//! println!("You have made {} moves", solitaire_game.moves());
//! // View the next card
//! match solitaire_game.peek() {
//!     Ok(card) => println!("Your next card is {}", card),
//!     Err(SolitaireError::OutOfRangeError) => println!("No more cards to peek at!"),
//!     _ => unreachable!()
//!
//! }
//! // Check if the game is a win
//! if solitaire_game.game_won() {
//!     println!("You win!")
//! } else {
//!     println!("Don't give up yet!")
//! }
//!
//! ```

mod cards;
/// Solitaire Errors
mod errors;
mod game;
pub(crate) mod utils;
pub use self::cards::SolitaireCard;
pub use self::errors::*;
pub use self::game::*;
