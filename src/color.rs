//! Color Codes used for cards

/// Colors
#[repr(C)]
#[derive(
    Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, serde::Serialize, serde::Deserialize, Hash,
)]
pub enum Color {
    /// Black
    Black,
    /// Red
    Red,
}
